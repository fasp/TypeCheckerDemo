package com.fasp.typecheckerdemo;

import com.fasp.typecheckerdemo.types.Car;
import com.fasp.typecheckerdemo.types.RaceCar;

		
/*		
 Background info:
 https://en.wikipedia.org/wiki/Liskov_substitution_principle
 Which is why this works:
 Vehicle vehicle = (Vehicle)new Car();
 and this one does not work (ClassCastException):
 Car car = (Car)new Vehicle();			
 */	
public class TypeChecker {

	public static void main(String[] args) {
		
		Car car = new Car();
		RaceCar raceCar = new RaceCar();
				
		testInstanceOf(car, raceCar);
		testAssignableFrom(car, raceCar);

		testInstanceOf(null, raceCar);
		testInstanceOf(car, null);

	}

	private static void testInstanceOf(Car car, RaceCar raceCar) {
		System.out.print("car is an instance of Car: ");
		System.out.println(car instanceof Car);  // true
		System.out.print("car is an instance of RaceCar: ");
		System.out.println(car instanceof RaceCar);  // false
		System.out.print("raceCar is an instance of Car: ");
		System.out.println(raceCar instanceof Car);  // true
		System.out.print("raceCar is an instance of RaceCar: ");
		System.out.println(raceCar instanceof RaceCar);  // true
		System.out.print("null is an instance of Car: ");
		System.out.println(null instanceof Car);  // false
	}

	private static void testAssignableFrom(Car car, RaceCar raceCar) {
		System.out.print("Car is assignable from Car: ");
		System.out.println(Car.class.isAssignableFrom(car.getClass()));  // true
		System.out.print("Car is assignable from RaceCar: ");
		System.out.println(Car.class.isAssignableFrom(raceCar.getClass()));  // true
		System.out.print("RaceCar is assignable from Car: ");
		System.out.println(RaceCar.class.isAssignableFrom(car.getClass()));  // false
		System.out.print("RaceCar is assignable from RaceCar: ");
		System.out.println(RaceCar.class.isAssignableFrom(raceCar.getClass()));  // true	
	}

}
