package com.fasp.typecheckerdemo.types;

public class RaceCar extends Car {
	
	@Override
	public void drive() {
		System.out.println("I'm driving very fast");
	}
}
